<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EmployeeController@index');

Route::get('/employee', 'EmployeeController@index');
Route::get('/create', 'EmployeeController@create');
Route::post('/create', 'EmployeeController@submit');
Route::get('/edit/{id}', 'EmployeeController@edit');
Route::post('/edit', 'EmployeeController@update');
Route::get('/delete/{id}', 'EmployeeController@delete');
Route::get('/detail/{id}', 'EmployeeController@detail');

Route::get('/company', 'CompanyController@index');
Route::get('/company/create', 'CompanyController@create');
Route::post('/company/create', 'CompanyController@submit');
Route::get('/company/edit/{id}', 'CompanyController@edit');
Route::post('/company/edit', 'CompanyController@update');
Route::get('/company/delete/{id}', 'CompanyController@delete');
Route::get('/company/detail/{id}', 'CompanyController@detail');
