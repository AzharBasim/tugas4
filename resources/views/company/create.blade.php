@extends('layout/main')

@section('title', 'Add Company')

@section('container')
<div class="container">
    <div class="row">
        <div class="col">
            <h1>Tambah Data</h1>
            <form action="/company/create" method="POST">
                @csrf
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Perusahaan">
                </div>
                <div class="mb-3">
                    <label for="alamat" class="form-label">Alamat</label>
                    <input type="text" name="alamat" class="form-control" id="alamat" placeholder="Alamat Perusahaan">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/company" class="btn btn-warning">Kembali</a>
            </form>
        </div>
    </div>
</div>
@endsection
