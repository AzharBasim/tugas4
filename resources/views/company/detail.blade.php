@extends('layout/main')

@section('title', 'Employees Detail')

@section('container')
<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="btn-close" aria-label="Close"></button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <div class="card mt-4" style="width: 20rem;">
                <div class="card-body">
                    <h5 class="card-title">Company Detail</h5>
                    <p class="card-text"><strong>Nama Perusahaan:</strong> {{ $company[0]->nama }}</p>
                    <p class="card-text"><strong>Alamat:</strong> {{ $company[0]->alamat }}</p>
                    <a href="/company/edit/{{ $company[0]->id }}" class="btn btn-success">Edit</a>
                    <a href="/company/delete/{{ $company[0]->id }}" class="btn btn-danger">Delete</a>
                    <a href="/company" class="btn btn-info"> Kembali</a>
           </div>
       </div>
   </div>
</div>



{{-- <form action="{{ $employee[0]>id }}" method="post" class="d-inline">
     @method('delete')
     @csrf
 <button type="submit" class="btn btn-danger">delete</button>
</form> --}}

@endsection
