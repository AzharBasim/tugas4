@extends('layout/main')

@section('title', 'Employees List')

@section('container')
<div class="container">

    <h1>Company List</h1>

    <div class="row">
        <div class="col">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block mt-3">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <table class ="table">
                <a href="/company/create" class="btn btn-primary my-8">Tambah Data</a>
                <thead class= "thead-dark">
                    <tr>
                        <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($companies as $company)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{$company->nama }}</td>
                        <td>{{$company->alamat }}</td>
                        <td><a href="/company/detail/{{$company->id }}" class="badge badge-info">Detail</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
