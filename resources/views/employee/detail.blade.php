@extends('layout/main')

@section('title', 'Employees Detail')

@section('container')
<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="btn-close" aria-label="Close"></button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <div class="card mt-4" style="width: 20rem;">
                <div class="card-body">
                    <h5 class="card-title">Employee Detail</h5>
                    <p class="card-text"><strong>Nama:</strong> {{ $employee[0]->nama }}</p>
                    <p class="card-text"><strong>Jabatan:</strong> {{ $employee[0]->atasan_id }}</p>
                    <p class="card-text"><strong>Company:</strong> {{ $employee[0]->company }}</p>
                    <a href="/edit/{{ $employee[0]->id }}" class="btn btn-success">Edit</a>
                    <a href="/delete/{{ $employee[0]->id }}" class="btn btn-danger">Delete</a>
                    <a href="/" class="btn btn-info"> Kembali</a>
           </div>
       </div>
   </div>
</div>



{{-- <form action="{{ $employee[0]>id }}" method="post" class="d-inline">
     @method('delete')
     @csrf
 <button type="submit" class="btn btn-danger">delete</button>
</form> --}}

@endsection
