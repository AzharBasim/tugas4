@extends('layout/main')

@section('title', 'Employees List')

@section('container')
<div class="container">

    <h1>Employee List</h1>

    <div class="row">
        <div class="col">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block mt-3">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <table class ="table">
                <a href="/create" class="btn btn-primary my-8">Tambah Data</a>
                <thead class= "thead-dark">
                    <tr>
                        <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">ID_Atasan</th>
                    <th scope="col">ID_Perusahaan</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employees as $employee)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $employee->nama }}</td>
                        <td>{{ $employee->atasan_id }}</td>
                        <td>{{ $employee->company_id }}</td>
                        <td><a href="/detail/{{ $employee->id }}" class="badge badge-info">Detail</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
