@extends('layout/main')

@section('title', 'Employees List')

@section('container')
<div class="container">
    <div class="row">
        <div class="col">
            <h1>Edit Data</h1>
            <form action="/edit" method="POST">
            @csrf
                <input type="hidden" name="id" value="{{ $employee[0]->id }}">
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" value="{{ $employee[0]->nama }}">
                </div>
                <div class="mb-3">
                    <label for="atasan_id" class="form-label">Atasan Id</label>
                    <input type="text" name="atasan_id" class="form-control" id="atasan_id" value="{{ $employee[0]->atasan_id }}">
                </div>
                <div class="mb-3">
                    <label for="company_id" class="form-label">Company Id</label>
                    <input type="text" name="company_id" class="form-control" id="company_id" value="{{ $employee[0]->company_id }}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/detail/{{ $employee[0]->id }}" class="btn btn-warning">Kembali</a>
            </form>
        </div>
    </div>
</div>
@endsection
