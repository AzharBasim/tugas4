@extends('layout/main')

@section('title', 'Add Employees')

@section('container')
<div class="container">
    <div class="row">
        <div class="col">
            <h1>Tambah Data</h1>
            <form action="/create" method="POST">
            @csrf
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Lengkap">
                </div>
                <div class="mb-3">
                    <label for="atasan_id" class="form-label">Atasan Id</label>
                    <input type="text" name="atasan_id" class="form-control" id="atasan_id" placeholder="Masukan angka 1 - 4">
                </div>
                <div class="mb-3">
                    <label for="company_id" class="form-label">Company Id</label>
                    <input type="text" name="company_id" class="form-control" id="company_id" placeholder="Masukan angka 1 - 2">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/" class="btn btn-warning">Kembali</a>
            </form>
        </div>
    </div>
</div>
@endsection
