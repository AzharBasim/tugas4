@extends('layout/main')

@section('title', 'List of Employees')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="mt-4">List of Employees</h1>
                <a href="/employee/create" class="btn btn-primary">Add (+)<a>

                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nama</th>
                        <th scope="col">ID Atasan</th>
                        <th scope="col">ID Perusahaan</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employee as $emp)
                    <tr>
                        <th scrope="row">{{$emp->id}}</th>
                        <td>{{$emp->nama}}</td>
                        <td>{{$emp->atasan_id}}</td>
                        <td>{{$emp->company_id}}</td>
                            <td>
                                <a href="" class="badge badge-success">edit<a>
                                <a href="" class="badge badge-danger">delete<a>
                            </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>

            </div>
        </div>
    </div>
@endsection
