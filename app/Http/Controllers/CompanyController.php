<?php

namespace App\Http\Controllers;


use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CompanyController extends BaseController
{
    public function index()
    {
        $companies = DB::table('company')->get();

        $data = [
            'companies' => $companies
        ];

        return view('company/index', $data);
    }

    public function create()
    {
        return view('company/create');
    }

    public function submit(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required'
        ]);

        DB::table('company')->insert(
            [
                'nama' => $request->nama,
                'alamat' => $request->alamat
            ]);

        return redirect('/company')->with(['success' => 'Data berhasil ditambahkan!']);
    }

    public function edit($id)
    {
        $company = DB::table('company')->where('id', $id)->get();

        $data = [
            'company' => $company
        ];

        return view('company/edit', $data);
    }

    public function update(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required'
        ]);

        DB::table('company')->where('id', $request->id)->update(
            [
                'nama' => $request->nama,
                'alamat' => $request->alamat
            ]
        );

        return back()->with(['success' => 'Data berhasil diupdate!']);
    }

    public function delete($id)
    {
        DB::table('company')->where('id', $id)->delete();

        return redirect('/company')->with(['success' => 'Data berhasil dihapus!']);
    }

    public function detail($id)
    {
        $company = DB::table('company')
        ->where('company.id', $id)
        ->get();
        return view('company/detail', ['company' => $company]);
    }
}
